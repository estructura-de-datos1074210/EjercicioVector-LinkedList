/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util;

/**
 *
 * @author profesor
 */
public class ListaS<T> {

    private Nodo<T> cabeza;
    private int size;

    public ListaS() {
        this.cabeza = null;
    }

    public void insertarInicio(T objeto) {
        Nodo<T> nuevo = new Nodo(objeto, this.cabeza);
        this.cabeza = nuevo;
        size++;
    }

    public boolean esVacio() {
        return this.cabeza == null;
    }

    public int getSize() {
        return size;

    }

    public String toString() {
        String msg = "cab<>";
        for (Nodo<T> i = this.cabeza; i != null; i = i.getSig()) {
            msg += i.getInfo().toString() + "->";
        }
        return msg + "null";
    }

    public boolean contains(T objeto) {
        if (objeto == null || this.esVacio()) {
            throw new RuntimeException("no puede buscar un elemento");
        }
        for (Nodo<T> i = this.cabeza; i != null; i = i.getSig()) {
            if (i.getInfo().equals(objeto)) {
                return true;
            }
        }
        return false;
    }

    public void clear() {
        this.cabeza = null;
        this.size = 0;
    }

    public void virus(T info, int n) {
        int repeticion = n;
        while (repeticion > 0) {
            insertarUltimo(info);
            repeticion--;
        }
    }

    public void insertarUltimo(T objeto) {
        Nodo<T> ultimo = new Nodo(objeto, null);
        if (objeto==null){
            throw new RuntimeException("No se esta ingresando ningun objeto");
        }
        if (this.esVacio()) {
            this.cabeza = ultimo;
        } else {
            Nodo<T> anterior;
            for (anterior = this.cabeza; anterior.getSig() != null; anterior = anterior.getSig()){
                anterior = anterior.getSig();
            }
            anterior.setSig(ultimo);
        }
        size++;
    }

}
